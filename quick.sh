#!/bin/bash


[[ -z $1 ]] && file=yaesu.csv || file=$1
cat FRS.csv \
    MURS.csv \
    NOAA.csv \
    US-call.csv \
    houston-tx-50.csv \
    austin-tx-50.csv \
    bastrop-tx-50.csv \
    burnet-tx-50.csv \
    leander-tx.csv \
    marine-vhf.csv \
    > $file
  f=
perl -i -pe 's/^\d+//' $f
perlCommonFunctions.pl -a removeDuplicates -f $f
perl -i -pe 's/(^)/($.-1)/e  if ($. > 1)' $f # renumber
