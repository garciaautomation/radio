#!/bin/bash
# pm=$(getPackageManager)
# $pm install chirp

installChirp(){
    sudo apt-add-repository ppa:dansmith/chirp-snapshots
    sudo apt-get update
    sudo apt-get install chirp-daily
    sudo apt install fldigi
}

install-gqrx(){
    sudo add-apt-repository -y ppa:bladerf/bladerf
    sudo add-apt-repository -y ppa:myriadrf/drivers
    sudo add-apt-repository -y ppa:myriadrf/gnuradio
    sudo add-apt-repository -y ppa:gqrx/gqrx-sdr
    sudo apt-get update
    sudo apt-get install gqrx-sdr
}

install-gqrx-scanner(){
    cd ~/programming
    git clone https://github.com/neural75/gqrx-scanner.git
    cd gqrx-scanner
    cmake ./
    make

    # gqrx set Tools Remote control
}

winlink-pat(){
    go get github.com/la5nta/pat
    pat configure
    pat http
}